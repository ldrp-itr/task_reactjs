import React, { Fragment, useEffect, useState } from 'react';
import { Card, Button } from 'react-bootstrap'

function Purchased() {
    const [service, setService] = useState([])

    useEffect(() => {
        fetch("https://jsonkeeper.com/b/356L").then((data) => {
            data.json().then(result => {
                console.log(result.data.purchased_services[0].name)
                setService(result)
            })
        })

    }, [])
    return (
        <Fragment>
            <h2>PURCHASED SERVICES</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <Card>
                <Card.Body>
                    <Card.Title>{console.log(service.data.purchased_services[0].name)}</Card.Title>
                    <Card.Text>
                        {service.data.purchased_services[0].id}
                    </Card.Text>
                    <Button variant="primary">Click to go next</Button>
                </Card.Body>
            </Card>
        </Fragment>
    )
}

export default Purchased;